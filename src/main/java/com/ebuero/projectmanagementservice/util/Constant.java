package com.ebuero.projectmanagementservice.util;

/**
 * @author S.shakery
 */

public class Constant {

    public static final int DEVELOPER_MAX_SP_PER_PLAN = 10;

    public static final String DEFAULT_UNEXPECTED_ERROR = "unexpected error";
    public static final String DEVELOPER_NOT_FOUND = "developer not found";
    public static final String DEVELOPER_ALREADY_INACTIVATED = "developer already inactivated";
    public static final String INVALID_DEVELOPER_STATUS = "invalid developer status";
    public static final String DUPLICATE_DEVELOPER_NAME = "developer name currently exist, try another name";

    public static final String BUG_ALREADY_DELETED = "bug already deleted";
    public static final String BUG_NOT_FOUND = "bug not found";
    public static final String INVALID_BUG_STATUS = "invalid bug status";
    public static final String INVALID_BUG_PRIORITY = "invalid bug priority";
    public static final String ADD_DELETED_BUG_IS_NOT_ALLOWED = "can not add bugs with deleted status";

    public static final String STORY_ALREADY_DELETED = "story already deleted";
    public static final String STORY_NOT_FOUND = "story not found";
    public static final String INVALID_STORY_STATUS = "invalid story status";
    public static final String GREATER_THAN_MAX_SP_PER_PLAN = "story estimated point is greater than valid maximum story point per plan for each developer, which is: " + DEVELOPER_MAX_SP_PER_PLAN;

    public static final String STORY_MUST_BECOME_ACTIVATED_FIRST = "stories need to become activated and assigned to developers for changing status to complete";
    public static final String NO_ACTIVE_DEVELOPER_FOUND = "No active developers found, you need to have some developers first";
    public static final String NO_ESTIMATED_STORY_FOUND = "No estimated stories found, you need to have some estimated stories first";

    public static final String INVALID_REQUEST = "request is invalid";
    public static final String REQUIRED_ID_PATH_VARIABLE = "ID is required";
    public static final String REQUIRED_TITLE = "title is required";
    public static final String REQUIRED_DESC = "desc is required";
    public static final String REQUIRED_NAME = "name is required";


}
