package com.ebuero.projectmanagementservice.controller;

import com.ebuero.projectmanagementservice.annotation.group.GroupCheck;
import com.ebuero.projectmanagementservice.dto.request.AddBugDTO;
import com.ebuero.projectmanagementservice.dto.request.EditBugDTO;
import com.ebuero.projectmanagementservice.dto.response.BugDTO;
import com.ebuero.projectmanagementservice.dto.response.ListResponseDTO;
import com.ebuero.projectmanagementservice.service.IBugService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@RestController
@RequestMapping(path = "/bug")
@Validated
public class BugController {

    private final IBugService service;

    public BugController(IBugService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public ResponseEntity<BugDTO> getBugById(@PathVariable Long id) {
        return new ResponseEntity<>(service.getBugById(id), HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<ListResponseDTO<BugDTO>> getAllBugs(@RequestParam(required = false, defaultValue = "0") int page,
                                                              @RequestParam(required = false, defaultValue = "15") int size,
                                                              @RequestParam(required = false, defaultValue = "99") int status){
        return new ResponseEntity<>(service.getAllBugs(page, size, status), HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<BugDTO> addBug(@Validated(GroupCheck.OrderedCheck.class)
                                         @RequestBody AddBugDTO bugDTO) {
        return new ResponseEntity<>(service.addBug(bugDTO), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BugDTO> deleteBug(@PathVariable Long id) {
        return new ResponseEntity<>(service.deleteBug(id), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<BugDTO> editBug(@Validated(GroupCheck.OrderedCheck.class)
                                          @RequestBody EditBugDTO bugDTO) {
        return new ResponseEntity<>(service.editBug(bugDTO), HttpStatus.OK);
    }
}
