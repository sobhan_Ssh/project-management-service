package com.ebuero.projectmanagementservice.controller;

import com.ebuero.projectmanagementservice.annotation.group.GroupCheck;
import com.ebuero.projectmanagementservice.dto.request.AddDeveloperDTO;
import com.ebuero.projectmanagementservice.dto.request.EditDeveloperDTO;
import com.ebuero.projectmanagementservice.dto.response.DeveloperDTO;
import com.ebuero.projectmanagementservice.dto.response.ListResponseDTO;
import com.ebuero.projectmanagementservice.service.IDeveloperService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/developer")
@Validated
public class DeveloperController {

    private final IDeveloperService service;

    public DeveloperController(IDeveloperService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public ResponseEntity<DeveloperDTO> getDeveloperById(@PathVariable Long id) {
        return new ResponseEntity<>(service.getDeveloperById(id), HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<ListResponseDTO<DeveloperDTO>> getAllDevelopers(@RequestParam(required = false, defaultValue = "0") int page,
                                                                          @RequestParam(required = false, defaultValue = "15") int size,
                                                                          @RequestParam(required = false, defaultValue = "99") int status) {
        return new ResponseEntity<>(service.getAllDevelopers(page, size, status), HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<DeveloperDTO> addDeveloper(@Validated(GroupCheck.OrderedCheck.class)
                                                     @RequestBody AddDeveloperDTO developerDTO) {
        return new ResponseEntity<>(service.addDeveloper(developerDTO), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<DeveloperDTO> deleteDeveloper(@PathVariable Long id) {
        return new ResponseEntity<>(service.deleteDeveloper(id), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<DeveloperDTO> editDeveloper(@Validated(GroupCheck.OrderedCheck.class)
                                                      @RequestBody EditDeveloperDTO developerDTO) {
        return new ResponseEntity<>(service.editDeveloper(developerDTO), HttpStatus.OK);
    }
}
