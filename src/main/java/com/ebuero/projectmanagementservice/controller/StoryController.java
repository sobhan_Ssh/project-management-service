package com.ebuero.projectmanagementservice.controller;

import com.ebuero.projectmanagementservice.annotation.group.GroupCheck;
import com.ebuero.projectmanagementservice.dto.request.AddStoryDTO;
import com.ebuero.projectmanagementservice.dto.request.EditStoryDTO;
import com.ebuero.projectmanagementservice.dto.response.ListResponseDTO;
import com.ebuero.projectmanagementservice.dto.response.StoryDTO;
import com.ebuero.projectmanagementservice.service.IStoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@RestController
@RequestMapping(path = "/story")
@Validated
public class StoryController {

    private final IStoryService service;

    public StoryController(IStoryService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public ResponseEntity<StoryDTO> getStoryById(@PathVariable
                                                 @NotNull Long id) {
        return new ResponseEntity<>(service.getStoryById(id), HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<ListResponseDTO<StoryDTO>> getAllStories(@RequestParam(required = false, defaultValue = "0") int page,
                                                                   @RequestParam(required = false, defaultValue = "15") int size,
                                                                   @RequestParam(required = false, defaultValue = "99") int status) {
        return new ResponseEntity<>(service.getAllStories(page, size, status), HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<StoryDTO> addStory(@Validated(GroupCheck.OrderedCheck.class)
                                             @RequestBody AddStoryDTO storyDTO) {
        return new ResponseEntity<>(service.addStory(storyDTO), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StoryDTO> deleteStory(@PathVariable
                                                @NotNull Long id) {
        return new ResponseEntity<>(service.deleteStory(id), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<StoryDTO> updateStory(@Validated(GroupCheck.OrderedCheck.class)
                                              @RequestBody EditStoryDTO storyDTO) {
        return new ResponseEntity<>(service.updateStory(storyDTO), HttpStatus.OK);
    }

    @PatchMapping("/complete/{id}")
    public ResponseEntity<StoryDTO> completeStory(@PathVariable
                                                  @NotNull Long id){
        return new ResponseEntity<>(service.completeStory(id), HttpStatus.OK);
    }
}
