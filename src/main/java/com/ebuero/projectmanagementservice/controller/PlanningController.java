package com.ebuero.projectmanagementservice.controller;

import com.ebuero.projectmanagementservice.dto.response.ListResponseDTO;
import com.ebuero.projectmanagementservice.dto.response.PlanDTO;
import com.ebuero.projectmanagementservice.service.IPlanningService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: s.shakeri
 * at 7/2/2022
 **/

@RestController
@RequestMapping(path = "/planning")
public class PlanningController {

    private final IPlanningService service;

    public PlanningController(IPlanningService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<ListResponseDTO<PlanDTO>> getPlanning() {
        return new ResponseEntity<>(service.getPlanning(), HttpStatus.OK);
    }
}
