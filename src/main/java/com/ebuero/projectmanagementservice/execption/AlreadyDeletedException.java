package com.ebuero.projectmanagementservice.execption;

public class AlreadyDeletedException extends RuntimeException {

    public AlreadyDeletedException(String s) {
        super(s);
    }
}
