package com.ebuero.projectmanagementservice.execption;

import org.springframework.http.HttpStatus;

/**
 * @Author: s.shakeri
 * at 7/3/2022
 **/

public class PlanException extends RuntimeException {

    public PlanException(String s) {
        super(s);
    }
}
