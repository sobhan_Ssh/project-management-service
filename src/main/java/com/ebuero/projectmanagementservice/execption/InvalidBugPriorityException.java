package com.ebuero.projectmanagementservice.execption;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

public class InvalidBugPriorityException extends RuntimeException {

    public InvalidBugPriorityException(String s) {
        super(s);
    }
}