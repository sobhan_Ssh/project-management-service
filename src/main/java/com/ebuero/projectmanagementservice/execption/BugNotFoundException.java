package com.ebuero.projectmanagementservice.execption;

import org.springframework.http.HttpStatus;

/**
 * @Author: s.shakeri
 * at 7/3/2022
 **/

public class BugNotFoundException extends RuntimeException {

    public BugNotFoundException(String s) {
        super(s);
    }
}
