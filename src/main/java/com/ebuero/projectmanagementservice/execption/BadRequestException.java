package com.ebuero.projectmanagementservice.execption;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

public class BadRequestException extends RuntimeException {

    public BadRequestException(String s) {
        super(s);
    }
}
