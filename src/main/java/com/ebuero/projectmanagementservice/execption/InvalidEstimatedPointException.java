package com.ebuero.projectmanagementservice.execption;

/**
 * @Author: s.shakeri
 * at 7/10/2022
 **/

public class InvalidEstimatedPointException extends RuntimeException {

    public InvalidEstimatedPointException(String s) {
        super(s);
    }
}
