package com.ebuero.projectmanagementservice.execption;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

public class InvalidDeveloperStatusException extends RuntimeException {

    public InvalidDeveloperStatusException(String s) {
        super(s);
    }
}