package com.ebuero.projectmanagementservice.execption;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

public class InvalidBugStatusException extends RuntimeException {

    public InvalidBugStatusException(String s) {
        super(s);
    }
}