package com.ebuero.projectmanagementservice.execption;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

public class InvalidStoryStatusException extends RuntimeException {

    public InvalidStoryStatusException(String s) {
        super(s);
    }
}