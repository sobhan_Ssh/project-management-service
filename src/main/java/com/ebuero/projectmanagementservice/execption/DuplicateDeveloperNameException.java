package com.ebuero.projectmanagementservice.execption;

/**
 * @Author: s.shakeri
 * at 7/14/2022
 **/

public class DuplicateDeveloperNameException extends RuntimeException {

    public DuplicateDeveloperNameException(String s) {
        super(s);
    }
}
