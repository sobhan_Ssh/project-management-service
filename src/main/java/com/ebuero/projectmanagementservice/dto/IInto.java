package com.ebuero.projectmanagementservice.dto;

public interface IInto<DestinationType> {

    DestinationType into(DestinationType destination);

}
