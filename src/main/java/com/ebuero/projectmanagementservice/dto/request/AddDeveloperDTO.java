package com.ebuero.projectmanagementservice.dto.request;

import com.ebuero.projectmanagementservice.annotation.AddDeveloperRequest;
import com.ebuero.projectmanagementservice.annotation.group.GroupCheck;
import com.ebuero.projectmanagementservice.util.Constant;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AddDeveloperRequest(groups = GroupCheck.ClassCheck.class)
public class AddDeveloperDTO {

    @NotEmpty(message = Constant.REQUIRED_NAME)
    @NotNull(message = Constant.REQUIRED_NAME)
    private String name;

    public String getName() {
        return name;
    }

    public AddDeveloperDTO setName(String name) {
        this.name = name;
        return this;
    }
}
