package com.ebuero.projectmanagementservice.dto.request;

import com.ebuero.projectmanagementservice.annotation.EditBugRequest;
import com.ebuero.projectmanagementservice.annotation.group.GroupCheck;
import com.ebuero.projectmanagementservice.enums.BugPriority;
import com.ebuero.projectmanagementservice.enums.BugStatus;
import com.ebuero.projectmanagementservice.util.Constant;

import javax.validation.constraints.NotNull;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@EditBugRequest(groups = GroupCheck.ClassCheck.class)
public class EditBugDTO {


    @NotNull(message = Constant.REQUIRED_ID_PATH_VARIABLE)
    private Long id;
    private String title;
    private String desc;
    private BugStatus status;
    private BugPriority priority;

    public Long getId() {
        return id;
    }

    public EditBugDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public EditBugDTO setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public EditBugDTO setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public BugStatus getStatus() {
        return status;
    }

    public EditBugDTO setStatus(BugStatus status) {
        this.status = status;
        return this;
    }

    public BugPriority getPriority() {
        return priority;
    }

    public EditBugDTO setPriority(BugPriority priority) {
        this.priority = priority;
        return this;
    }
}
