package com.ebuero.projectmanagementservice.dto.request;

import com.ebuero.projectmanagementservice.annotation.AddStoryRequest;
import com.ebuero.projectmanagementservice.annotation.group.GroupCheck;
import com.ebuero.projectmanagementservice.util.Constant;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@AddStoryRequest(groups = GroupCheck.ClassCheck.class)
public class AddStoryDTO {

    @NotEmpty(message = Constant.REQUIRED_TITLE)
    @NotNull(message = Constant.REQUIRED_TITLE)
    private String title;

    @NotEmpty(message = Constant.REQUIRED_DESC)
    @NotNull(message = Constant.REQUIRED_DESC)
    private String desc;

    private Integer estimatedPoint;

    public String getTitle() {
        return title;
    }

    public AddStoryDTO setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public AddStoryDTO setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public Integer getEstimatedPoint() {
        return estimatedPoint;
    }

    public AddStoryDTO setEstimatedPoint(Integer estimatedPoint) {
        this.estimatedPoint = estimatedPoint;
        return this;
    }

}
