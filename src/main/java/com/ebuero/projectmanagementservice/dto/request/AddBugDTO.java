package com.ebuero.projectmanagementservice.dto.request;

import com.ebuero.projectmanagementservice.annotation.AddBugRequest;
import com.ebuero.projectmanagementservice.annotation.BugPriority;
import com.ebuero.projectmanagementservice.annotation.BugStatus;
import com.ebuero.projectmanagementservice.annotation.group.AdvancedCheck;
import com.ebuero.projectmanagementservice.annotation.group.GroupCheck;
import com.ebuero.projectmanagementservice.util.Constant;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@AddBugRequest(groups = GroupCheck.ClassCheck.class)
public class AddBugDTO {

    @NotEmpty(message = Constant.REQUIRED_TITLE)
    @NotNull(message = Constant.REQUIRED_TITLE)
    private String title;

    @NotEmpty(message = Constant.REQUIRED_DESC)
    @NotNull(message = Constant.REQUIRED_DESC)
    private String desc;

    @BugStatus(groups = AdvancedCheck.class)
    private Long status;

    @BugPriority(groups = AdvancedCheck.class)
    private Long priority;

    public String getTitle() {
        return title;
    }

    public AddBugDTO setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public AddBugDTO setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public Long getStatus() {
        return status;
    }

    public AddBugDTO setStatus(Long status) {
        this.status = status;
        return this;
    }

    public Long getPriority() {
        return priority;
    }

    public AddBugDTO setPriority(Long priority) {
        this.priority = priority;
        return this;
    }
}
