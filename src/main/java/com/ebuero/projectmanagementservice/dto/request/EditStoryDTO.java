package com.ebuero.projectmanagementservice.dto.request;

import com.ebuero.projectmanagementservice.annotation.EditStoryRequest;
import com.ebuero.projectmanagementservice.annotation.group.GroupCheck;
import com.ebuero.projectmanagementservice.enums.StoryStatus;
import com.ebuero.projectmanagementservice.util.Constant;

import javax.validation.constraints.NotNull;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@EditStoryRequest(groups = GroupCheck.ClassCheck.class)
public class EditStoryDTO {

    @NotNull(message = Constant.REQUIRED_ID_PATH_VARIABLE)
    private Long id;
    private String title;
    private String desc;
    private Integer estimatedPoint;

    public Long getId() {
        return id;
    }

    public EditStoryDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public EditStoryDTO setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public EditStoryDTO setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public Integer getEstimatedPoint() {
        return estimatedPoint;
    }

    public EditStoryDTO setEstimatedPoint(Integer estimatedPoint) {
        this.estimatedPoint = estimatedPoint;
        return this;
    }
}
