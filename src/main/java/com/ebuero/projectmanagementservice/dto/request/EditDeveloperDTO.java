package com.ebuero.projectmanagementservice.dto.request;

import com.ebuero.projectmanagementservice.annotation.EditDeveloperRequest;
import com.ebuero.projectmanagementservice.annotation.group.GroupCheck;
import com.ebuero.projectmanagementservice.util.Constant;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@EditDeveloperRequest(groups = GroupCheck.ClassCheck.class)
public class EditDeveloperDTO {

    @NotNull(message = Constant.REQUIRED_ID_PATH_VARIABLE)
    private Long id;
    @NotEmpty(message = Constant.REQUIRED_NAME)
    @NotNull(message = Constant.REQUIRED_NAME)
    private String name;

    public Long getId() {
        return id;
    }

    public EditDeveloperDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public EditDeveloperDTO setName(String name) {
        this.name = name;
        return this;
    }
}
