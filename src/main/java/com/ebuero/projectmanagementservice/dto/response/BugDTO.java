package com.ebuero.projectmanagementservice.dto.response;

import com.ebuero.projectmanagementservice.dto.IFrom;
import com.ebuero.projectmanagementservice.entity.Bug;
import com.ebuero.projectmanagementservice.enums.BugPriority;
import com.ebuero.projectmanagementservice.enums.BugStatus;

import java.sql.Timestamp;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

public class BugDTO implements IFrom<Bug> {

    private Long id;
    private String title;
    private String desc;
    private Timestamp creationDate;
    private BugStatus status;
    private BugPriority priority;

    public Long getId() {
        return id;
    }

    public BugDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public BugDTO setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public BugDTO setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public BugDTO setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public BugStatus getStatus() {
        return status;
    }

    public BugDTO setStatus(BugStatus status) {
        this.status = status;
        return this;
    }

    public BugPriority getPriority() {
        return priority;
    }

    public BugDTO setPriority(BugPriority priority) {
        this.priority = priority;
        return this;
    }

    @Override
    public BugDTO from(Bug source) {
        return this
                .setCreationDate(source.getCreationDate())
                .setDesc(source.getDesc())
                .setId(source.getId())
                .setTitle(source.getTitle())
                .setStatus(source.getStatus())
                .setPriority(source.getPriority());
    }
}
