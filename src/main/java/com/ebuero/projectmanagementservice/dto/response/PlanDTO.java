package com.ebuero.projectmanagementservice.dto.response;

import com.ebuero.projectmanagementservice.dto.IFrom;
import com.ebuero.projectmanagementservice.entity.ActiveStory;
import com.ebuero.projectmanagementservice.entity.Plan;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class PlanDTO implements IFrom<Plan> {

    private Timestamp startDate;
    private Timestamp endDate;
    List<ActiveStoryDTO> storiesAssignments = new ArrayList<>();

    public Timestamp getStartDate() {
        return startDate;
    }

    public PlanDTO setStartDate(Timestamp startDate) {
        this.startDate = startDate;
        return this;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public PlanDTO setEndDate(Timestamp endDate) {
        this.endDate = endDate;
        return this;
    }

    public List<ActiveStoryDTO> getStoriesAssignments() {
        return storiesAssignments;
    }

    public PlanDTO setStoriesAssignments(List<ActiveStoryDTO> storiesAssignments) {
        this.storiesAssignments = storiesAssignments;
        return this;
    }

    @Override
    public PlanDTO from(Plan source) {
        this
                .setStartDate(source.getStartDate())
                .setEndDate(source.getEndDate());
        for (ActiveStory activeStory : source.getActiveStories()) {
            ActiveStoryDTO activeStoryDTO = new ActiveStoryDTO().from(activeStory);
            this.getStoriesAssignments().add(activeStoryDTO);
        }
        return this;
    }
}
