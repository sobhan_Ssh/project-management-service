package com.ebuero.projectmanagementservice.dto.response;

import com.ebuero.projectmanagementservice.dto.IFrom;
import com.ebuero.projectmanagementservice.entity.ActiveStory;

public class ActiveStoryDTO implements IFrom<ActiveStory> {

    private String developerName;
    private String storyTitle;
    private String storyDesc;
    private Integer storyPoint;

    public String getDeveloperName() {
        return developerName;
    }

    public ActiveStoryDTO setDeveloperName(String developerName) {
        this.developerName = developerName;
        return this;
    }

    public String getStoryTitle() {
        return storyTitle;
    }

    public ActiveStoryDTO setStoryTitle(String storyTitle) {
        this.storyTitle = storyTitle;
        return this;
    }

    public String getStoryDesc() {
        return storyDesc;
    }

    public ActiveStoryDTO setStoryDesc(String storyDesc) {
        this.storyDesc = storyDesc;
        return this;
    }

    public Integer getStoryPoint() {
        return storyPoint;
    }

    public ActiveStoryDTO setStoryPoint(Integer storyPoint) {
        this.storyPoint = storyPoint;
        return this;
    }

    @Override
    public ActiveStoryDTO from(ActiveStory source) {
        return this
                .setStoryDesc(source.getStory().getDesc())
                .setStoryTitle(source.getStory().getTitle())
                .setStoryPoint(source.getStory().getEstimatedPoint())
                .setDeveloperName(source.getDeveloper().getName());
    }
}
