package com.ebuero.projectmanagementservice.dto.response;

import com.ebuero.projectmanagementservice.dto.IFrom;
import com.ebuero.projectmanagementservice.entity.Story;
import com.ebuero.projectmanagementservice.enums.StoryStatus;

import java.sql.Timestamp;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

public class StoryDTO implements IFrom<Story> {

    private Long id;
    private String title;
    private String desc;
    private Timestamp creationDate;
    private Integer estimatedPoint;
    private StoryStatus status;

    public Long getId() {
        return id;
    }

    public StoryDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public StoryDTO setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public StoryDTO setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public StoryDTO setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public Integer getEstimatedPoint() {
        return estimatedPoint;
    }

    public StoryDTO setEstimatedPoint(Integer estimatedPoint) {
        this.estimatedPoint = estimatedPoint;
        return this;
    }

    public StoryStatus getStatus() {
        return status;
    }

    public StoryDTO setStatus(StoryStatus status) {
        this.status = status;
        return this;
    }

    @Override
    public StoryDTO from(Story source) {
        return this
                .setCreationDate(source.getCreationDate())
                .setDesc(source.getDesc())
                .setId(source.getId())
                .setTitle(source.getTitle())
                .setStatus(source.getStatus())
                .setEstimatedPoint(source.getEstimatedPoint());
    }
}
