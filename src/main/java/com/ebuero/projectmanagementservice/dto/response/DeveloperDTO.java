package com.ebuero.projectmanagementservice.dto.response;

import com.ebuero.projectmanagementservice.dto.IFrom;
import com.ebuero.projectmanagementservice.entity.Developer;
import com.ebuero.projectmanagementservice.enums.DeveloperStatus;

public class DeveloperDTO implements IFrom<Developer> {

    private Long id;
    private String name;
    private DeveloperStatus status;

    public Long getId() {
        return id;
    }

    public DeveloperDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public DeveloperDTO setName(String name) {
        this.name = name;
        return this;
    }

    public DeveloperStatus getStatus() {
        return status;
    }

    public DeveloperDTO setStatus(DeveloperStatus status) {
        this.status = status;
        return this;
    }

    @Override
    public DeveloperDTO from(Developer source) {
        return this
                .setId(source.getId())
                .setStatus(source.getStatus())
                .setName(source.getName());
    }
}
