package com.ebuero.projectmanagementservice.dto.service;

import com.ebuero.projectmanagementservice.entity.Developer;
import com.ebuero.projectmanagementservice.entity.Story;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: s.shakeri
 * at 7/10/2022
 **/

public class DeveloperStoryPointDTO {

    private Developer developer;
    private List<Story> storyList = new ArrayList<>();
    private Integer point = 0;

    public DeveloperStoryPointDTO(Developer developer) {
        this.developer = developer;
        this.point = 0;
        this.storyList = new ArrayList<>();
    }

    public DeveloperStoryPointDTO() {
    }

    public Developer getDeveloper() {
        return developer;
    }

    public DeveloperStoryPointDTO setDeveloper(Developer developer) {
        this.developer = developer;
        return this;
    }

    public List<Story> getStoryList() {
        return storyList;
    }

    public DeveloperStoryPointDTO setStoryList(List<Story> storyList) {
        this.storyList = storyList;
        return this;
    }

    public Integer getPoint() {
        return point;
    }

    public DeveloperStoryPointDTO setPoint(Integer point) {
        this.point = point;
        return this;
    }
}
