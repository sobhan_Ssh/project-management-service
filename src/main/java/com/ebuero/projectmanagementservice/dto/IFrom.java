package com.ebuero.projectmanagementservice.dto;

public interface IFrom<SourceType> {

    IFrom<SourceType> from(SourceType source);

}
