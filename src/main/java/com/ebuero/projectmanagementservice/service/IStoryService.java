package com.ebuero.projectmanagementservice.service;

import com.ebuero.projectmanagementservice.dto.request.AddStoryDTO;
import com.ebuero.projectmanagementservice.dto.request.EditStoryDTO;
import com.ebuero.projectmanagementservice.dto.response.ListResponseDTO;
import com.ebuero.projectmanagementservice.dto.response.StoryDTO;
import com.ebuero.projectmanagementservice.entity.Story;

import java.util.List;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

public interface IStoryService {

    List<Story> getEstimatedStories();

    StoryDTO getStoryById(Long id);

    ListResponseDTO<StoryDTO> getAllStories(int page, int size, int status);

    StoryDTO addStory(AddStoryDTO storyDTO);

    StoryDTO deleteStory(Long id);

    StoryDTO updateStory(EditStoryDTO storyDTO);

    StoryDTO completeStory(Long id);

    StoryDTO activateStory(Long id);
}
