package com.ebuero.projectmanagementservice.service;

import com.ebuero.projectmanagementservice.dto.request.AddBugDTO;
import com.ebuero.projectmanagementservice.dto.request.EditBugDTO;
import com.ebuero.projectmanagementservice.dto.response.BugDTO;
import com.ebuero.projectmanagementservice.dto.response.ListResponseDTO;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

public interface IBugService {

    BugDTO getBugById(Long id);

    ListResponseDTO<BugDTO> getAllBugs(int page, int size, int status);

    BugDTO addBug(AddBugDTO bugDTO);

    BugDTO deleteBug(Long id);

    BugDTO editBug(EditBugDTO bugDTO);
}
