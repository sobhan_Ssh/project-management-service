package com.ebuero.projectmanagementservice.service.impl;

import com.ebuero.projectmanagementservice.dto.request.AddDeveloperDTO;
import com.ebuero.projectmanagementservice.dto.request.EditDeveloperDTO;
import com.ebuero.projectmanagementservice.dto.response.DeveloperDTO;
import com.ebuero.projectmanagementservice.dto.response.ListResponseDTO;
import com.ebuero.projectmanagementservice.entity.Developer;
import com.ebuero.projectmanagementservice.enums.DeveloperStatus;
import com.ebuero.projectmanagementservice.execption.AlreadyDeletedException;
import com.ebuero.projectmanagementservice.execption.DeveloperNotFoundException;
import com.ebuero.projectmanagementservice.execption.DuplicateDeveloperNameException;
import com.ebuero.projectmanagementservice.repository.DeveloperRepository;
import com.ebuero.projectmanagementservice.service.IDeveloperService;
import com.ebuero.projectmanagementservice.util.Constant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class DeveloperServiceImpl implements IDeveloperService {

    private final DeveloperRepository repository;

    public DeveloperServiceImpl(DeveloperRepository repository) {
        this.repository = repository;
    }

    private Developer findDeveloperById(Long id) {
        return repository
                .findById(id)
                .orElseThrow(() -> new DeveloperNotFoundException(Constant.DEVELOPER_NOT_FOUND));
    }

    private boolean checkDeveloperNameUniqueness(String name) {
        var tmp = repository.findByNameLike(name);
        if (tmp.isEmpty())
            return true;
        else
            throw new DuplicateDeveloperNameException(Constant.DUPLICATE_DEVELOPER_NAME);
    }

    @Override
    public List<Developer> getActiveDevelopers() {
        var temp = repository.findByStatus(DeveloperStatus.ACTIVE);
        return temp.orElseGet(ArrayList::new);
    }

    @Override
    public DeveloperDTO getDeveloperById(Long id) {
        return new DeveloperDTO().from(findDeveloperById(id));
    }

    @Override
    public ListResponseDTO<DeveloperDTO> getAllDevelopers(int page, int size, int status) {
        ListResponseDTO<DeveloperDTO> response = new ListResponseDTO<>();
        Page<Developer> temp;
        if (Objects.equals(status, 99))
            temp = repository.findAll(PageRequest.of(page, size));
        else
            temp = repository.findByStatus(DeveloperStatus.parseDeveloperStatus(status), PageRequest.of(page, size));

        if (!temp.isEmpty()) {
            response = new ListResponseDTO<>(page, temp.getNumberOfElements());
            for (Developer developer : temp)
                response.getData().add(new DeveloperDTO().from(developer));
        }
        return response;
    }

    @Override
    public DeveloperDTO addDeveloper(AddDeveloperDTO developerDTO) {
        checkDeveloperNameUniqueness(developerDTO.getName());
        return new DeveloperDTO()
                .from(repository.save(new Developer()
                        .setName(developerDTO.getName())
                        .setStatus(DeveloperStatus.ACTIVE)));
    }

    @Override
    public DeveloperDTO deleteDeveloper(Long id) {
        var temp = findDeveloperById(id);
        if (Objects.equals(temp.getStatus(), DeveloperStatus.INACTIVE))
            throw new AlreadyDeletedException(Constant.DEVELOPER_ALREADY_INACTIVATED);
        return new DeveloperDTO().from(repository.save(temp.setStatus(DeveloperStatus.INACTIVE)));
    }

    @Override
    public DeveloperDTO editDeveloper(EditDeveloperDTO developerDTO) {
        var temp = findDeveloperById(developerDTO.getId());
        if (Objects.equals(temp.getStatus(), DeveloperStatus.INACTIVE))
            throw new AlreadyDeletedException(Constant.DEVELOPER_ALREADY_INACTIVATED);
        checkDeveloperNameUniqueness(developerDTO.getName());
        return new DeveloperDTO().from(repository.save(temp.setName(developerDTO.getName())));
    }
}
