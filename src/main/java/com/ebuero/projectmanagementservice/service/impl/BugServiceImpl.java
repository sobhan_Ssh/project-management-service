package com.ebuero.projectmanagementservice.service.impl;

import com.ebuero.projectmanagementservice.dto.request.AddBugDTO;
import com.ebuero.projectmanagementservice.dto.request.EditBugDTO;
import com.ebuero.projectmanagementservice.dto.response.BugDTO;
import com.ebuero.projectmanagementservice.dto.response.ListResponseDTO;
import com.ebuero.projectmanagementservice.entity.Bug;
import com.ebuero.projectmanagementservice.entity.Developer;
import com.ebuero.projectmanagementservice.enums.BugStatus;
import com.ebuero.projectmanagementservice.execption.AlreadyDeletedException;
import com.ebuero.projectmanagementservice.execption.BugNotFoundException;
import com.ebuero.projectmanagementservice.execption.DeveloperNotFoundException;
import com.ebuero.projectmanagementservice.execption.InvalidBugStatusException;
import com.ebuero.projectmanagementservice.repository.BugRepository;
import com.ebuero.projectmanagementservice.service.IBugService;
import com.ebuero.projectmanagementservice.util.Constant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@Service
public class BugServiceImpl implements IBugService {

    private final BugRepository repository;

    public BugServiceImpl(BugRepository repository) {
        this.repository = repository;
    }

    private Bug findBugById(Long id) {
        return repository
                .findById(id)
                .orElseThrow(() -> new BugNotFoundException(Constant.BUG_NOT_FOUND));
    }

    @Override
    public BugDTO getBugById(Long id) {
        return new BugDTO().from(findBugById(id));
    }

    @Override
    public ListResponseDTO<BugDTO> getAllBugs(int page, int size, int status) {
        ListResponseDTO<BugDTO> response = new ListResponseDTO<>();
        Page<Bug> temp;
        if (Objects.equals(status, 99))
            temp = repository.findAll(PageRequest.of(page, size));
        else
            temp = repository.findByStatus(BugStatus.parseBugStatus(status), PageRequest.of(page, size));

        if (!temp.isEmpty()) {
            response = new ListResponseDTO<>(page, temp.getNumberOfElements());
            for (Bug bug : temp)
                response.getData().add(new BugDTO().from(bug));
        }
        return response;
    }

    @Override
    public BugDTO addBug(AddBugDTO bugDTO) {
        if (Objects.equals(BugStatus.DELETED.getValue(), bugDTO.getStatus()))
            throw new InvalidBugStatusException(Constant.ADD_DELETED_BUG_IS_NOT_ALLOWED);
        return new BugDTO().from(repository.save(new Bug().from(bugDTO)));
    }

    @Override
    public BugDTO deleteBug(Long id) {
        var temp = findBugById(id);
        if (Objects.equals(temp.getStatus(), BugStatus.DELETED))
            throw new AlreadyDeletedException(Constant.BUG_ALREADY_DELETED);
        return new BugDTO().from(repository.save(temp.setStatus(BugStatus.DELETED)));
    }

    @Override
    public BugDTO editBug(EditBugDTO bugDTO) {
        var temp = findBugById(bugDTO.getId());
        if (Objects.equals(temp.getStatus(), BugStatus.DELETED))
            throw new AlreadyDeletedException(Constant.BUG_ALREADY_DELETED);
        else {
            if (Objects.nonNull(bugDTO.getTitle()) && !bugDTO.getTitle().isEmpty())
                temp.setTitle(bugDTO.getTitle());
            if (Objects.nonNull(bugDTO.getStatus()))
                temp.setStatus(bugDTO.getStatus());
            if (Objects.nonNull(bugDTO.getDesc()) && !bugDTO.getDesc().isEmpty())
                temp.setDesc(bugDTO.getDesc());
            if (Objects.nonNull(bugDTO.getPriority()))
                temp.setPriority(bugDTO.getPriority());
            return new BugDTO().from(repository.save(temp));
        }
    }
}
