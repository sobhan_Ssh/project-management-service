package com.ebuero.projectmanagementservice.service.impl;

import com.ebuero.projectmanagementservice.dto.response.ListResponseDTO;
import com.ebuero.projectmanagementservice.dto.response.PlanDTO;
import com.ebuero.projectmanagementservice.dto.service.DeveloperStoryPointDTO;
import com.ebuero.projectmanagementservice.entity.ActiveStory;
import com.ebuero.projectmanagementservice.entity.Developer;
import com.ebuero.projectmanagementservice.entity.Plan;
import com.ebuero.projectmanagementservice.entity.Story;
import com.ebuero.projectmanagementservice.execption.DeveloperNotFoundException;
import com.ebuero.projectmanagementservice.execption.StoryNotFoundException;
import com.ebuero.projectmanagementservice.repository.ActiveStoryRepository;
import com.ebuero.projectmanagementservice.repository.PlanRepository;
import com.ebuero.projectmanagementservice.service.IDeveloperService;
import com.ebuero.projectmanagementservice.service.IPlanningService;
import com.ebuero.projectmanagementservice.service.IStoryService;
import com.ebuero.projectmanagementservice.util.Constant;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * @Author: s.shakeri
 * at 7/2/2022
 **/

@Service
public class PlanningServiceImpl implements IPlanningService {

    private final PlanRepository repository;
    private final ActiveStoryRepository activeStoryRepository;
    private final IDeveloperService developerService;
    private final IStoryService storyService;

    public PlanningServiceImpl(PlanRepository repository, ActiveStoryRepository activeStoryRepository, IDeveloperService developerService, IStoryService storyService) {
        this.repository = repository;
        this.activeStoryRepository = activeStoryRepository;
        this.developerService = developerService;
        this.storyService = storyService;
    }

    private List<Developer> getActiveDevelopers() {
        return developerService.getActiveDevelopers();
    }

    private List<Story> getEstimatedStories() {
        return storyService.getEstimatedStories();
    }

    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public ListResponseDTO<PlanDTO> getPlanning() {
        List<Developer> activeDevelopers = getActiveDevelopers();
        if (activeDevelopers.size() > 0) {
            List<Story> estimatedStories = getEstimatedStories();
            if (estimatedStories.size() > 0) {
                ListResponseDTO<PlanDTO> responseDTO = new ListResponseDTO<>();
                List<Plan> plans = createPlans(activeDevelopers, estimatedStories);
                if (plans.size() > 0) {
                    responseDTO.setPage(0).setSize(plans.size());
                    for (Plan plan : plans)
                        responseDTO.getData().add(new PlanDTO().from(plan));
                }
                return responseDTO;
            } else
                throw new StoryNotFoundException(Constant.NO_ESTIMATED_STORY_FOUND);
        } else
            throw new DeveloperNotFoundException(Constant.NO_ACTIVE_DEVELOPER_FOUND);
    }

    private List<Plan> createPlans(List<Developer> developers, List<Story> stories) {
        double maxPointsPerPlan = developers.size() * Constant.DEVELOPER_MAX_SP_PER_PLAN;
        double totalPoints = stories.stream().mapToDouble(Story::getEstimatedPoint).sum();
        int numberOfRequiredPlans = totalPoints <= maxPointsPerPlan ? 1 : (int) Math.ceil(totalPoints / maxPointsPerPlan);
        List<Plan> plans = new ArrayList<>();
        var maxPlanEndDate = repository.findPlanHavingMaxEndDate();
        for (int i = 0; i < numberOfRequiredPlans; i++) {
            Plan plan = new Plan();
            plan.setActiveStories(createActiveStoriesGreedy(developers, stories, plan));
            plan.setStartDate(maxPlanEndDate.isPresent() ?
                    Timestamp.valueOf(maxPlanEndDate.get().getEndDate().toLocalDateTime().plus(i, ChronoUnit.WEEKS)) :
                    Timestamp.valueOf(LocalDateTime.now().plus(i, ChronoUnit.WEEKS)));
            plan.setEndDate(maxPlanEndDate.isPresent() ?
                    Timestamp.valueOf(maxPlanEndDate.get().getEndDate().toLocalDateTime().plus(i + 1, ChronoUnit.WEEKS)) :
                    Timestamp.valueOf(LocalDateTime.now().plus(i + 1, ChronoUnit.WEEKS)));
            repository.save(plan);
            saveActiveStories(plan.getActiveStories());
            plans.add(plan);
        }
        return plans;
    }

    private List<ActiveStory> createActiveStoriesEnhanced(List<Developer> developers, List<Story> stories, Plan plan) {
        List<ActiveStory> activeStories = new ArrayList<>();
        int maxPointsPerPlan = developers.size() * Constant.DEVELOPER_MAX_SP_PER_PLAN;
        int currentPlanTotalPoints = 0;
        PriorityQueue<DeveloperStoryPointDTO> pq = createPriorityQueueForDevelopers(developers);
        while (pq.size() > 0 && stories.size() > 0 && currentPlanTotalPoints <= maxPointsPerPlan) {
            stories.sort(Comparator.comparing(Story::getEstimatedPoint, Collections.reverseOrder()));
            for (int i = 0; i < pq.size(); i++) {
                DeveloperStoryPointDTO developer1 = pq.poll();
                Story story = stories.get(i);
            }
            DeveloperStoryPointDTO developerStoryPointDTO = pq.poll();
            ActiveStory activeStory = new ActiveStory()
                    .setDeveloper(developerStoryPointDTO.getDeveloper())
                    .setStory(stories.stream().max(Comparator.comparing(Story::getEstimatedPoint)).get());
            activateStory(activeStory.getStory());
            developerStoryPointDTO.setPoint(developerStoryPointDTO.getPoint() + activeStory.getStory().getEstimatedPoint());
            if (developerStoryPointDTO.getPoint() < Constant.DEVELOPER_MAX_SP_PER_PLAN)
                pq.add(developerStoryPointDTO);
            activeStory.setPlan(plan);
            activeStories.add(activeStory);
        }
        return activeStories;
    }

    private List<ActiveStory> createActiveStoriesGreedy(List<Developer> developers, List<Story> stories, Plan plan) {
        stories.sort(Comparator.comparing(Story::getEstimatedPoint).reversed());
        List<DeveloperStoryPointDTO> developerStoryPointDTOList = new ArrayList<>();
        for (Developer developer : developers) {
            DeveloperStoryPointDTO developerStoryPointDTO = new DeveloperStoryPointDTO();
            developerStoryPointDTO.setDeveloper(developer);
            for (Iterator<Story> iterator = stories.iterator(); iterator.hasNext(); ) {
                Story story = iterator.next();
                if (developerStoryPointDTO.getPoint() + story.getEstimatedPoint() <= Constant.DEVELOPER_MAX_SP_PER_PLAN) {
                    iterator.remove();
                    developerStoryPointDTO.getStoryList().add(story);
                    developerStoryPointDTO.setPoint(developerStoryPointDTO.getPoint() + story.getEstimatedPoint());
                    activateStory(story);
                }
            }
            developerStoryPointDTOList.add(developerStoryPointDTO);
        }
        return createActiveStoriesFromDeveloperStoryPointDTOList(developerStoryPointDTOList, plan);
    }

    private List<ActiveStory> createActiveStoriesFromDeveloperStoryPointDTOList(List<DeveloperStoryPointDTO> developerStoryPointDTOList, Plan plan) {
        List<ActiveStory> activeStories = new ArrayList<>();
        for (DeveloperStoryPointDTO developerStoryPointDTO : developerStoryPointDTOList) {
            Developer developer = developerStoryPointDTO.getDeveloper();
            for (Story story : developerStoryPointDTO.getStoryList()) {
                ActiveStory activeStory = new ActiveStory();
                activeStory.setStory(story);
                activeStory.setDeveloper(developer);
                activeStory.setPlan(plan);
                activeStories.add(activeStory);
            }
        }
        return activeStories;
    }

    private List<ActiveStory> saveActiveStories(List<ActiveStory> activeStories) {
        return activeStoryRepository.saveAll(activeStories);
    }


    private PriorityQueue<DeveloperStoryPointDTO> createPriorityQueueForDevelopers(List<Developer> developers) {
        PriorityQueue<DeveloperStoryPointDTO> pq = new PriorityQueue(developers.size(), Comparator.comparing(DeveloperStoryPointDTO::getPoint));
        for (Developer developer : developers) {
            pq.add(new DeveloperStoryPointDTO(developer));
        }
        return pq;
    }

    private void activateStory(Story story) {
        storyService.activateStory(story.getId());
    }


}
