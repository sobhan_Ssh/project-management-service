package com.ebuero.projectmanagementservice.service.impl;

import com.ebuero.projectmanagementservice.dto.request.AddStoryDTO;
import com.ebuero.projectmanagementservice.dto.request.EditStoryDTO;
import com.ebuero.projectmanagementservice.dto.response.ListResponseDTO;
import com.ebuero.projectmanagementservice.dto.response.StoryDTO;
import com.ebuero.projectmanagementservice.entity.Story;
import com.ebuero.projectmanagementservice.enums.StoryStatus;
import com.ebuero.projectmanagementservice.execption.AlreadyDeletedException;
import com.ebuero.projectmanagementservice.execption.InvalidEstimatedPointException;
import com.ebuero.projectmanagementservice.execption.InvalidStoryStatusException;
import com.ebuero.projectmanagementservice.execption.StoryNotFoundException;
import com.ebuero.projectmanagementservice.repository.StoryRepository;
import com.ebuero.projectmanagementservice.service.IStoryService;
import com.ebuero.projectmanagementservice.util.Constant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@Service
public class StoryServiceImpl implements IStoryService {

    private final StoryRepository repository;

    public StoryServiceImpl(StoryRepository repository) {
        this.repository = repository;
    }

    private Story findDStoryById(Long id) {
        return repository
                .findById(id)
                .orElseThrow(() -> new StoryNotFoundException(Constant.STORY_NOT_FOUND));
    }

    @Override
    public List<Story> getEstimatedStories() {
        return repository.findByStatus(StoryStatus.ESTIMATED).orElseGet(ArrayList::new);
    }

    @Override
    public StoryDTO getStoryById(Long id) {
        return new StoryDTO().from(findDStoryById(id));
    }

    @Override
    public ListResponseDTO<StoryDTO> getAllStories(int page, int size, int status) {
        ListResponseDTO<StoryDTO> response = new ListResponseDTO<>();
        Page<Story> temp;
        if (Objects.equals(status, 99))
            temp = repository.findAll(PageRequest.of(page, size));
        else
            temp = repository.findByStatus(StoryStatus.parseStoryStatus(status), PageRequest.of(page, size));
        if (!temp.isEmpty()) {
            response = new ListResponseDTO<>(page, temp.getNumberOfElements());
            for (Story story : temp)
                response.getData().add(new StoryDTO().from(story));
        }
        return response;
    }

    @Override
    public StoryDTO addStory(AddStoryDTO storyDTO) {
        if (Objects.nonNull(storyDTO.getEstimatedPoint()))
            isEstimatedPointValid(storyDTO.getEstimatedPoint());
        return new StoryDTO().from(repository.save(new Story().from(storyDTO)));
    }

    @Override
    public StoryDTO deleteStory(Long id) {
        var temp = findDStoryById(id);
        if (Objects.equals(temp.getStatus(), StoryStatus.DELETED))
            throw new AlreadyDeletedException(Constant.STORY_ALREADY_DELETED);
        return new StoryDTO().from(repository.save(temp.setStatus(StoryStatus.DELETED)));
    }

    @Override
    public StoryDTO updateStory(EditStoryDTO storyDTO) {
        var temp = findDStoryById(storyDTO.getId());
        if (Objects.equals(temp.getStatus(), StoryStatus.DELETED))
            throw new AlreadyDeletedException(Constant.STORY_ALREADY_DELETED);
        else {
            if (Objects.nonNull(storyDTO.getDesc()) && !storyDTO.getDesc().isEmpty())
                temp.setDesc(storyDTO.getDesc());
            if (Objects.nonNull(storyDTO.getTitle()) && !storyDTO.getTitle().isEmpty())
                temp.setTitle(storyDTO.getTitle());
            if (Objects.nonNull(storyDTO.getEstimatedPoint())) {
                isEstimatedPointValid(storyDTO.getEstimatedPoint());
                temp.setEstimatedPoint(storyDTO.getEstimatedPoint());
            }
            return new StoryDTO().from(repository.save(temp));
        }
    }

    private void isEstimatedPointValid(Integer estimatedPoint) {
        if (estimatedPoint > 10)
            throw new InvalidEstimatedPointException(Constant.GREATER_THAN_MAX_SP_PER_PLAN);
    }

    @Override
    public StoryDTO completeStory(Long id) {
        var temp = findDStoryById(id);
        if (!Objects.equals(temp.getStatus(), StoryStatus.ACTIVATED))
            throw new InvalidStoryStatusException(Constant.STORY_MUST_BECOME_ACTIVATED_FIRST);
        return new StoryDTO().from(repository.save(temp.setStatus(StoryStatus.COMPLETED)));
    }

    @Override
    public StoryDTO activateStory(Long id) {
        var temp = findDStoryById(id);
        return new StoryDTO().from(repository.save(temp.setStatus(StoryStatus.ACTIVATED)));
    }
}
