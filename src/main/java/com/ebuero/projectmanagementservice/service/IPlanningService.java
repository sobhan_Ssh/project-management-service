package com.ebuero.projectmanagementservice.service;

import com.ebuero.projectmanagementservice.dto.response.ListResponseDTO;
import com.ebuero.projectmanagementservice.dto.response.PlanDTO;

/**
 * @Author: s.shakeri
 * at 7/2/2022
 **/

public interface IPlanningService {

    ListResponseDTO<PlanDTO> getPlanning();
}
