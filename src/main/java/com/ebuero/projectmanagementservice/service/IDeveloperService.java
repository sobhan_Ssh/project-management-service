package com.ebuero.projectmanagementservice.service;

import com.ebuero.projectmanagementservice.dto.request.AddDeveloperDTO;
import com.ebuero.projectmanagementservice.dto.request.EditDeveloperDTO;
import com.ebuero.projectmanagementservice.dto.response.DeveloperDTO;
import com.ebuero.projectmanagementservice.dto.response.ListResponseDTO;
import com.ebuero.projectmanagementservice.entity.Developer;

import java.util.List;

public interface IDeveloperService {

    DeveloperDTO getDeveloperById(Long id);

    ListResponseDTO<DeveloperDTO> getAllDevelopers(int page, int size, int status);

    DeveloperDTO addDeveloper(AddDeveloperDTO developerDTO);

    DeveloperDTO deleteDeveloper(Long id);

    DeveloperDTO editDeveloper(EditDeveloperDTO developerDTO);

    List<Developer> getActiveDevelopers();
}

