package com.ebuero.projectmanagementservice.repository;

import com.ebuero.projectmanagementservice.entity.Developer;
import com.ebuero.projectmanagementservice.enums.DeveloperStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DeveloperRepository extends JpaRepository<Developer, Long> {

    Page<Developer> findByStatus(DeveloperStatus status, Pageable page);
    Optional<List<Developer>> findByStatus(DeveloperStatus status);
    Optional<Developer> findByNameLike(String name);
}
