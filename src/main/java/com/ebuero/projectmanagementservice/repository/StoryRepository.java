package com.ebuero.projectmanagementservice.repository;

import com.ebuero.projectmanagementservice.entity.Story;
import com.ebuero.projectmanagementservice.enums.StoryStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@Repository
public interface StoryRepository extends JpaRepository<Story, Long> {

    Page<Story> findByStatus(StoryStatus status, Pageable page);
    Optional<List<Story>> findByStatus(StoryStatus status);
}
