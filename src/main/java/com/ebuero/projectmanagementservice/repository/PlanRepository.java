package com.ebuero.projectmanagementservice.repository;

import com.ebuero.projectmanagementservice.entity.Plan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @Author: s.shakeri
 * at 7/2/2022
 **/

@Repository
public interface PlanRepository extends JpaRepository<Plan, Long> {

    @Query("select p1 from Plan p1 where p1.endDate = (select max(p2.endDate) from Plan p2)")
    Optional <Plan> findPlanHavingMaxEndDate();
}
