package com.ebuero.projectmanagementservice.repository;

import com.ebuero.projectmanagementservice.entity.Bug;
import com.ebuero.projectmanagementservice.enums.BugStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@Repository
public interface BugRepository extends JpaRepository<Bug, Long> {

    Page<Bug> findByStatus(BugStatus status, Pageable page);
}
