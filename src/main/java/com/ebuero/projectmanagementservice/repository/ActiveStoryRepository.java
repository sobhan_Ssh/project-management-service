package com.ebuero.projectmanagementservice.repository;

import com.ebuero.projectmanagementservice.entity.ActiveStory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author: s.shakeri
 * at 7/2/2022
 **/

@Repository
public interface ActiveStoryRepository extends JpaRepository<ActiveStory, Long> {
}
