package com.ebuero.projectmanagementservice;

import com.ebuero.projectmanagementservice.config.ExceptionTranslator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(ExceptionTranslator.class)
public class ProjectManagementServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectManagementServiceApplication.class, args);
    }

}
