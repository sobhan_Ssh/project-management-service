package com.ebuero.projectmanagementservice.config;

import com.ebuero.projectmanagementservice.execption.*;
import com.ebuero.projectmanagementservice.util.Constant;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class ExceptionTranslator extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(Exception ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setDate(new Date());
        response.setMessage(Constant.DEFAULT_UNEXPECTED_ERROR);
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BugNotFoundException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(BugNotFoundException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(Constant.BUG_NOT_FOUND);
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DeveloperNotFoundException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(DeveloperNotFoundException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DuplicateDeveloperNameException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(DuplicateDeveloperNameException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.FAILED_DEPENDENCY);
    }

    @ExceptionHandler(StoryNotFoundException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(StoryNotFoundException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PlanException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(PlanException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InvalidBugStatusException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(InvalidBugStatusException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(InvalidBugPriorityException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(InvalidBugPriorityException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(InvalidDeveloperStatusException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(InvalidDeveloperStatusException
                                                                              ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(InvalidStoryStatusException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(InvalidStoryStatusException
                                                                              ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(BadRequestException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(BadRequestException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AlreadyDeletedException.class)
    protected ResponseEntity<DefaultResponse> handleUncaughtException(AlreadyDeletedException ex, WebRequest request) {
        DefaultResponse response = new DefaultResponse();
        response.setMessage(ex.getMessage());
        response.setDate(new Date());
        return new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
    }

}

