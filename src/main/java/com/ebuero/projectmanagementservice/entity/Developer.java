package com.ebuero.projectmanagementservice.entity;

import com.ebuero.projectmanagementservice.enums.DeveloperStatus;

import javax.persistence.*;

@Entity
@Table(name = "DEVELOPER")
public class Developer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "STATUS", nullable = false)
    private DeveloperStatus status;

    public Long getId() {
        return id;
    }

    public Developer setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Developer setName(String name) {
        this.name = name;
        return this;
    }

    public DeveloperStatus getStatus() {
        return status;
    }

    public Developer setStatus(DeveloperStatus status) {
        this.status = status;
        return this;
    }
}
