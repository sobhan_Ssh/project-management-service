package com.ebuero.projectmanagementservice.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@MappedSuperclass
public class Issue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column(name = "DESC", nullable = false)
    private String desc;

    @Column(name = "CREATE_DATE", nullable = false)
    private Timestamp creationDate;

    public Long getId() {
        return id;
    }

    public Issue setId(Long id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Issue setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public Issue setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public Issue setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
        return this;
    }
}
