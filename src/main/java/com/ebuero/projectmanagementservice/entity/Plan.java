package com.ebuero.projectmanagementservice.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "PLAN")
public class Plan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "START_DATE", nullable = false)
    private Timestamp startDate;

    @Column(name = "END_DATE", nullable = false)
    private Timestamp endDate;

    @OneToMany(mappedBy="story")
    private List<ActiveStory> activeStories;

    public Long getId() {
        return id;
    }

    public Plan setId(Long id) {
        this.id = id;
        return this;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public Plan setStartDate(Timestamp startDate) {
        this.startDate = startDate;
        return this;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public Plan setEndDate(Timestamp endDate) {
        this.endDate = endDate;
        return this;
    }

    public List<ActiveStory> getActiveStories() {
        return activeStories;
    }

    public Plan setActiveStories(List<ActiveStory> activeStories) {
        this.activeStories = activeStories;
        return this;
    }
}
