package com.ebuero.projectmanagementservice.entity;

import com.ebuero.projectmanagementservice.dto.IFrom;
import com.ebuero.projectmanagementservice.dto.request.AddStoryDTO;
import com.ebuero.projectmanagementservice.enums.StoryStatus;
import com.ebuero.projectmanagementservice.util.Utility;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@Entity
@Table(name = "STORY")
public class Story extends Issue implements IFrom<AddStoryDTO> {

    @Column(name = "POINT", nullable = true)
    private Integer estimatedPoint;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "STATUS", nullable = false)
    private StoryStatus status;

    @OneToMany(mappedBy="story")
    private List<ActiveStory> activeStories;

    public Integer getEstimatedPoint() {
        return estimatedPoint;
    }

    public Story setEstimatedPoint(Integer estimatedPoint) {
        this.estimatedPoint = estimatedPoint;
        return this;
    }

    public StoryStatus getStatus() {
        return status;
    }

    public Story setStatus(StoryStatus status) {
        this.status = status;
        return this;
    }

    public List<ActiveStory> getActiveStories() {
        return activeStories;
    }

    public Story setActiveStories(List<ActiveStory> activeStories) {
        this.activeStories = activeStories;
        return this;
    }

    @Override
    public Story from(AddStoryDTO source) {
        return (Story) this
                .setEstimatedPoint(source.getEstimatedPoint())
                .setStatus(Objects.nonNull(source.getEstimatedPoint()) ? StoryStatus.ESTIMATED : StoryStatus.NEW)
                .setCreationDate(Utility.getCurrentDate())
                .setDesc(source.getDesc())
                .setTitle(source.getTitle());
    }
}
