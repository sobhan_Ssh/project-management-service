package com.ebuero.projectmanagementservice.entity;

import javax.persistence.*;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@Entity
@Table(name = "ACTIVE_STORY")
public class ActiveStory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name="story_id", nullable=false)
    private Story story;

    @ManyToOne(optional = false)
    @JoinColumn(name="developer_id", nullable=false)
    private Developer developer;

    @ManyToOne(optional = false)
    @JoinColumn(name="plan_id", nullable=false)
    private Plan plan;

    public Long getId() {
        return id;
    }

    public ActiveStory setId(Long id) {
        this.id = id;
        return this;
    }

    public Story getStory() {
        return story;
    }

    public ActiveStory setStory(Story story) {
        this.story = story;
        return this;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public ActiveStory setDeveloper(Developer developer) {
        this.developer = developer;
        return this;
    }

    public Plan getPlan() {
        return plan;
    }

    public ActiveStory setPlan(Plan plan) {
        this.plan = plan;
        return this;
    }
}
