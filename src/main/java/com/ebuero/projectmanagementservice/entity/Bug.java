package com.ebuero.projectmanagementservice.entity;

import com.ebuero.projectmanagementservice.dto.IFrom;
import com.ebuero.projectmanagementservice.dto.request.AddBugDTO;
import com.ebuero.projectmanagementservice.enums.BugPriority;
import com.ebuero.projectmanagementservice.enums.BugStatus;
import com.ebuero.projectmanagementservice.util.Utility;

import javax.persistence.*;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

@Entity
@Table(name = "BUG")
public class Bug extends Issue implements IFrom<AddBugDTO> {

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "STATUS", nullable = false)
    private BugStatus status;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "PRIORITY", nullable = false)
    private BugPriority priority;

    public BugStatus getStatus() {
        return status;
    }

    public Bug setStatus(BugStatus status) {
        this.status = status;
        return this;
    }

    public BugPriority getPriority() {
        return priority;
    }

    public Bug setPriority(BugPriority priority) {
        this.priority = priority;
        return this;
    }

    @Override
    public Bug from(AddBugDTO source) {
        return (Bug) this
                .setPriority(BugPriority.parseBugPriority(Math.toIntExact(source.getPriority())))
                .setStatus(BugStatus.parseBugStatus(Math.toIntExact(source.getStatus())))
                .setCreationDate(Utility.getCurrentDate())
                .setDesc(source.getDesc())
                .setTitle(source.getTitle());
    }
}
