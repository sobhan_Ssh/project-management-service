package com.ebuero.projectmanagementservice.enums;

/**
 * @Author: s.shakeri
 * at 7/2/2022
 **/

public enum  PlanStatus {

    SCHEDULED(0),
    STARTED(1),
    FINISHED(2),
    DELETED(3);

    private final int value;

    PlanStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static boolean contains(int value) {
        // Check if enum contain text then return true else return false
        for (PlanStatus status : PlanStatus.values()) {
            if (status.getValue() == value) {
                return true;
            }
        }
        return false;
    }
}
