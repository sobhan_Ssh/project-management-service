package com.ebuero.projectmanagementservice.enums;

import com.ebuero.projectmanagementservice.execption.InvalidBugPriorityException;
import com.ebuero.projectmanagementservice.execption.InvalidBugStatusException;
import com.ebuero.projectmanagementservice.util.Constant;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

public enum BugPriority {

    CRITICAL(0),
    MAJOR(1),
    MINOR(2);

    private final int value;

    BugPriority(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static boolean contains(int value) {
        // Check if enum contain text then return true else return false
        for (BugStatus status : BugStatus.values()) {
            if (status.getValue() == value) {
                return true;
            }
        }
        return false;
    }

    public static BugPriority parseBugPriority(int status) {
        if (status == BugPriority.CRITICAL.getValue())
            return BugPriority.CRITICAL;
        else if (status == BugPriority.MAJOR.getValue())
            return BugPriority.MAJOR;
        else if (status == BugPriority.MINOR.getValue())
            return BugPriority.MINOR;
        else
            throw new InvalidBugPriorityException(Constant.INVALID_BUG_PRIORITY);
    }
}
