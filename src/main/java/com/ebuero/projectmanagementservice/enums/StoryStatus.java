package com.ebuero.projectmanagementservice.enums;

import com.ebuero.projectmanagementservice.execption.InvalidStoryStatusException;
import com.ebuero.projectmanagementservice.util.Constant;

public enum StoryStatus {

    NEW(0),
    ESTIMATED(1),
    ACTIVATED(2),
    COMPLETED(3),
    DELETED(4);

    private final int value;

    StoryStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static boolean contains(int value) {
        // Check if enum contain text then return true else return false
        for (StoryStatus status : StoryStatus.values()) {
            if (status.getValue() == value) {
                return true;
            }
        }
        return false;
    }

    public static StoryStatus parseStoryStatus(int status) {
        if (status == StoryStatus.NEW.getValue())
            return StoryStatus.NEW;
        else if (status == StoryStatus.ESTIMATED.getValue())
            return StoryStatus.ESTIMATED;
        else if (status == StoryStatus.COMPLETED.getValue())
            return StoryStatus.COMPLETED;
        else if (status == StoryStatus.DELETED.getValue())
            return StoryStatus.DELETED;
        else if (status == StoryStatus.ACTIVATED.getValue())
            return StoryStatus.ACTIVATED;
        else
            throw new InvalidStoryStatusException(Constant.INVALID_STORY_STATUS);
    }
}
