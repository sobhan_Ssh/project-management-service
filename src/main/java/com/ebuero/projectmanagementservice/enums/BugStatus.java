package com.ebuero.projectmanagementservice.enums;

import com.ebuero.projectmanagementservice.execption.InvalidBugStatusException;
import com.ebuero.projectmanagementservice.util.Constant;

/**
 * @Author: s.shakeri
 * at 6/30/2022
 **/

public enum BugStatus {

    NEW(0),
    VERIFIED(1),
    RESOLVED(2),
    DELETED(3);

    private final int value;

    BugStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static boolean contains(int value) {
        // Check if enum contain text then return true else return false
        for (BugStatus status : BugStatus.values()) {
            if (status.getValue() == value) {
                return true;
            }
        }
        return false;
    }

    public static BugStatus parseBugStatus(int status) {
        if (status == BugStatus.NEW.getValue())
            return BugStatus.NEW;
        else if (status == BugStatus.VERIFIED.getValue())
            return BugStatus.VERIFIED;
        else if (status == BugStatus.RESOLVED.getValue())
            return BugStatus.RESOLVED;
        else if (status == BugStatus.DELETED.getValue())
            return BugStatus.DELETED;
        else
            throw new InvalidBugStatusException(Constant.INVALID_BUG_STATUS);
    }
}
