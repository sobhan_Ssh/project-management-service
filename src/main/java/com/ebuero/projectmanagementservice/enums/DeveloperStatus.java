package com.ebuero.projectmanagementservice.enums;

import com.ebuero.projectmanagementservice.execption.InvalidDeveloperStatusException;
import com.ebuero.projectmanagementservice.util.Constant;

public enum DeveloperStatus {

    ACTIVE(0),
    INACTIVE(1);

    private final int value;

    DeveloperStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static boolean contains(int value) {
        // Check if enum contain text then return true else return false
        for (DeveloperStatus status : DeveloperStatus.values()) {
            if (status.getValue() == value) {
                return true;
            }
        }
        return false;
    }

    public static DeveloperStatus parseDeveloperStatus(int status) {
        if (status == DeveloperStatus.ACTIVE.getValue())
            return DeveloperStatus.ACTIVE;
        else if (status == DeveloperStatus.INACTIVE.getValue())
            return DeveloperStatus.INACTIVE;
        else
            throw new InvalidDeveloperStatusException(Constant.INVALID_DEVELOPER_STATUS);
    }
}
