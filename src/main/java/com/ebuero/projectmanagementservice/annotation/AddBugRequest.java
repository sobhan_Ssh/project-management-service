package com.ebuero.projectmanagementservice.annotation;

import com.ebuero.projectmanagementservice.annotation.validator.AddBugRequestValidator;
import com.ebuero.projectmanagementservice.util.Constant;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AddBugRequestValidator.class)
public @interface AddBugRequest {

    String message() default Constant.INVALID_REQUEST;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
