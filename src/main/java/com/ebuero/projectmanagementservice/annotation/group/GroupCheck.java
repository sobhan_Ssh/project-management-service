package com.ebuero.projectmanagementservice.annotation.group;

import javax.validation.GroupSequence;
import javax.validation.groups.Default;

public class GroupCheck {

    public interface ClassCheck {

    }

    @GroupSequence({Default.class, AdvancedCheck.class, ClassCheck.class})
    public interface OrderedCheck {
    }
}
