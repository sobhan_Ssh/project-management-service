package com.ebuero.projectmanagementservice.annotation;

import com.ebuero.projectmanagementservice.annotation.validator.DeveloperStatusValidator;
import com.ebuero.projectmanagementservice.util.Constant;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

@Target(ElementType.FIELD)
@Retention(RUNTIME)
@Constraint(validatedBy = DeveloperStatusValidator.class)
public @interface DeveloperStatus {
    String message() default Constant.INVALID_DEVELOPER_STATUS;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
