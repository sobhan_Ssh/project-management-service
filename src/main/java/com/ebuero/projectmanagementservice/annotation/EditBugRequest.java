package com.ebuero.projectmanagementservice.annotation;

import com.ebuero.projectmanagementservice.annotation.validator.EditBugRequestValidator;
import com.ebuero.projectmanagementservice.util.Constant;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EditBugRequestValidator.class)
public @interface EditBugRequest {
    String message() default Constant.INVALID_REQUEST;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
