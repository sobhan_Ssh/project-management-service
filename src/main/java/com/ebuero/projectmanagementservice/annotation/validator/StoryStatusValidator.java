package com.ebuero.projectmanagementservice.annotation.validator;

import com.ebuero.projectmanagementservice.annotation.StoryStatus;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

public class StoryStatusValidator implements ConstraintValidator<StoryStatus, Long> {
    @Override
    public void initialize(StoryStatus constraintAnnotation) {
    }

    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return Objects.nonNull(value) && com.ebuero.projectmanagementservice.enums.StoryStatus.contains(Math.toIntExact(value));
    }
}
