package com.ebuero.projectmanagementservice.annotation.validator;

import com.ebuero.projectmanagementservice.annotation.EditDeveloperRequest;
import com.ebuero.projectmanagementservice.dto.request.EditDeveloperDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class EditDeveloperRequestValidator implements ConstraintValidator<EditDeveloperRequest, EditDeveloperDTO> {
    @Override
    public void initialize(EditDeveloperRequest constraintAnnotation) {

    }

    @Override
    public boolean isValid(EditDeveloperDTO value, ConstraintValidatorContext context) {
        return Objects.nonNull(value.getName()) && Objects.nonNull(value.getId());
    }
}
