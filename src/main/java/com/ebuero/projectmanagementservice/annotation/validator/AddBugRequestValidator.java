package com.ebuero.projectmanagementservice.annotation.validator;

import com.ebuero.projectmanagementservice.annotation.AddBugRequest;
import com.ebuero.projectmanagementservice.dto.request.AddBugDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

public class AddBugRequestValidator implements ConstraintValidator<AddBugRequest, AddBugDTO> {
    @Override
    public void initialize(AddBugRequest constraintAnnotation) {

    }

    @Override
    public boolean isValid(AddBugDTO value, ConstraintValidatorContext context) {
        return Objects.nonNull(value.getDesc()) && Objects.nonNull(value.getTitle()) &&
                Objects.nonNull(value.getStatus()) && Objects.nonNull(value.getPriority());
    }
}
