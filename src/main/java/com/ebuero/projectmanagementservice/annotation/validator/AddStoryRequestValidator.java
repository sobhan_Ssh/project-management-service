package com.ebuero.projectmanagementservice.annotation.validator;

import com.ebuero.projectmanagementservice.annotation.AddStoryRequest;
import com.ebuero.projectmanagementservice.dto.request.AddStoryDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

public class AddStoryRequestValidator implements ConstraintValidator<AddStoryRequest, AddStoryDTO> {
    @Override
    public void initialize(AddStoryRequest constraintAnnotation) {

    }

    @Override
    public boolean isValid(AddStoryDTO value, ConstraintValidatorContext context) {
        return Objects.nonNull(value.getDesc()) && Objects.nonNull(value.getTitle());
    }
}
