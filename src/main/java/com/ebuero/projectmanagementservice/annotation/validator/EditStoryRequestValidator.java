package com.ebuero.projectmanagementservice.annotation.validator;

import com.ebuero.projectmanagementservice.annotation.EditStoryRequest;
import com.ebuero.projectmanagementservice.dto.request.EditStoryDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class EditStoryRequestValidator implements ConstraintValidator<EditStoryRequest, EditStoryDTO> {
    @Override
    public void initialize(EditStoryRequest constraintAnnotation) {

    }

    @Override
    public boolean isValid(EditStoryDTO value, ConstraintValidatorContext context) {
        return Objects.nonNull(value.getId());
    }
}
