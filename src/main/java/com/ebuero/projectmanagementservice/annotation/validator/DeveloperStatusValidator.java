package com.ebuero.projectmanagementservice.annotation.validator;

import com.ebuero.projectmanagementservice.annotation.DeveloperStatus;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

public class DeveloperStatusValidator implements ConstraintValidator<DeveloperStatus, Long> {
    @Override
    public void initialize(DeveloperStatus constraintAnnotation) {
    }

    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return Objects.nonNull(value) && com.ebuero.projectmanagementservice.enums.DeveloperStatus.contains(Math.toIntExact(value));
    }
}
