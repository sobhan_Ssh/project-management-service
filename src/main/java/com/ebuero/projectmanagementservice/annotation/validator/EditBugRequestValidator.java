package com.ebuero.projectmanagementservice.annotation.validator;

import com.ebuero.projectmanagementservice.annotation.EditBugRequest;
import com.ebuero.projectmanagementservice.dto.request.EditBugDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class EditBugRequestValidator implements ConstraintValidator<EditBugRequest, EditBugDTO> {
    @Override
    public void initialize(EditBugRequest constraintAnnotation) {

    }

    @Override
    public boolean isValid(EditBugDTO value, ConstraintValidatorContext context) {
        return Objects.nonNull(value.getId());
    }
}
