package com.ebuero.projectmanagementservice.annotation.validator;

import com.ebuero.projectmanagementservice.annotation.BugStatus;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

public class BugStatusValidator implements ConstraintValidator<BugStatus, Long> {
    @Override
    public void initialize(BugStatus constraintAnnotation) {
    }

    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return Objects.nonNull(value) && com.ebuero.projectmanagementservice.enums.BugStatus.contains(Math.toIntExact(value));
    }
}
