package com.ebuero.projectmanagementservice.annotation.validator;

import com.ebuero.projectmanagementservice.annotation.BugPriority;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

public class BugPriorityValidator implements ConstraintValidator<BugPriority, Long> {
    @Override
    public void initialize(BugPriority constraintAnnotation) {

    }

    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return Objects.nonNull(value) && com.ebuero.projectmanagementservice.enums.BugPriority.contains(Math.toIntExact(value));
    }
}
