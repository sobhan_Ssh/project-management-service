package com.ebuero.projectmanagementservice.annotation.validator;

import com.ebuero.projectmanagementservice.annotation.AddDeveloperRequest;
import com.ebuero.projectmanagementservice.dto.request.AddDeveloperDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 7/4/2022
 **/

public class AddDeveloperRequestValidator implements ConstraintValidator<AddDeveloperRequest, AddDeveloperDTO> {
    @Override
    public void initialize(AddDeveloperRequest constraintAnnotation) {

    }

    @Override
    public boolean isValid(AddDeveloperDTO value, ConstraintValidatorContext context) {
        return Objects.nonNull(value.getName());
    }
}
