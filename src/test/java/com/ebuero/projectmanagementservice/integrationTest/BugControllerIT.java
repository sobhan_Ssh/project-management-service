package com.ebuero.projectmanagementservice.integrationTest;

import com.ebuero.projectmanagementservice.dto.request.AddBugDTO;
import com.ebuero.projectmanagementservice.dto.request.EditBugDTO;
import com.ebuero.projectmanagementservice.dto.request.EditDeveloperDTO;
import com.ebuero.projectmanagementservice.dto.response.DeveloperDTO;
import com.ebuero.projectmanagementservice.entity.Bug;
import com.ebuero.projectmanagementservice.entity.Developer;
import com.ebuero.projectmanagementservice.entity.Issue;
import com.ebuero.projectmanagementservice.enums.BugPriority;
import com.ebuero.projectmanagementservice.enums.BugStatus;
import com.ebuero.projectmanagementservice.enums.DeveloperStatus;
import com.ebuero.projectmanagementservice.repository.BugRepository;
import com.ebuero.projectmanagementservice.util.Utility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class BugControllerIT {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private BugRepository bugRepository;
    @Autowired
    private ObjectMapper objectMapper;
    @BeforeEach
    void setup() {
        bugRepository.deleteAll();
    }

    @Test
    public void givenBugObject_whenCreateBug_thenReturnSavedBug() throws Exception {
        // given - create bug dto object
        AddBugDTO bugDTO = new AddBugDTO()
                .setTitle("title")
                .setDesc("desc")
                .setStatus((long) BugStatus.NEW.getValue())
                .setPriority((long) BugPriority.CRITICAL.getValue());

        // when - call '/bug/add' API
        ResultActions response = mockMvc.perform(post("/bug/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(bugDTO)));

        // then - verify the result content using assert statements
        response
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title", is(bugDTO.getTitle())))
                .andExpect(jsonPath("$.desc", is(bugDTO.getDesc())))
                .andExpect(jsonPath("$.status", is(BugStatus.NEW.name())))
                .andExpect(jsonPath("$.priority", is(BugPriority.CRITICAL.name())));
    }

    @Test
    public void givenListOfBugs_whenGetAllBugs_thenReturnBugsList() throws Exception {
        // given - create bugs list entity
        List<Bug> bugList = new ArrayList<>();
        Bug bug1 = new Bug();
        bug1.setCreationDate(Utility.getCurrentDate()).setDesc("desc1").setTitle("title1");
        bug1.setPriority(BugPriority.CRITICAL).setStatus(BugStatus.NEW);
        bugList.add(bug1);
        Bug bug2 = new Bug();
        bug2.setCreationDate(Utility.getCurrentDate()).setDesc("desc2").setTitle("title2");
        bug2.setPriority(BugPriority.MAJOR).setStatus(BugStatus.NEW);
        bugList.add(bug2);
        bugList = bugRepository.saveAll(bugList);

        // when -  call '/bug/list' API
        ResultActions response = mockMvc.perform(get("/bug/list"));

        // then - verify the result list size
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data.length()", is(bugList.size())));
    }

    @Test
    public void givenBugId_whenGetBugById_thenReturnBugObject() throws Exception{
        // given - create bug entity object
        Bug bug = new Bug();
        bug.setCreationDate(Utility.getCurrentDate()).setDesc("desc").setTitle("title");
        bug.setPriority(BugPriority.CRITICAL).setStatus(BugStatus.NEW);
        bug = bugRepository.save(bug);

        // when -  call '/bug/{id}' API
        ResultActions response = mockMvc.perform(get("/bug/{id}", bug.getId()));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.title", is(bug.getTitle())))
                .andExpect(jsonPath("$.desc", is(bug.getDesc())))
                .andExpect(jsonPath("$.status", is(BugStatus.NEW.name())))
                .andExpect(jsonPath("$.priority", is(BugPriority.CRITICAL.name())));
    }

    @Test
    public void givenUpdatedBug_whenUpdateBug_thenReturnUpdateBugObject() throws Exception{
        // given - create Bug entity object
        Bug bug = new Bug();
        bug.setCreationDate(Utility.getCurrentDate()).setDesc("desc").setTitle("title");
        bug.setPriority(BugPriority.CRITICAL).setStatus(BugStatus.NEW);
        bug = bugRepository.save(bug);

        // given - create edit bug dto
        EditBugDTO editBugDTO = new EditBugDTO()
                .setId(bug.getId())
                .setStatus(BugStatus.RESOLVED)
                .setPriority(BugPriority.MINOR)
                .setDesc("description")
                .setTitle("another title");

        // when -  call '/bug' API
        ResultActions response = mockMvc.perform(put("/bug")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(editBugDTO)));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.title", is(editBugDTO.getTitle())))
                .andExpect(jsonPath("$.desc", is(editBugDTO.getDesc())))
                .andExpect(jsonPath("$.status", is(BugStatus.RESOLVED.name())))
                .andExpect(jsonPath("$.priority", is(BugPriority.MINOR.name())));
    }

    @Test
    public void givenBugId_whenDeleteBug_thenReturnDeleteBugObject() throws Exception{
        // given - create bug entity object
        Bug bug = new Bug();
        bug.setCreationDate(Utility.getCurrentDate()).setDesc("desc").setTitle("title");
        bug.setPriority(BugPriority.CRITICAL).setStatus(BugStatus.NEW);
        bug = bugRepository.save(bug);

        // when -  call '/bug' API
        ResultActions response = mockMvc.perform(delete("/bug/{id}", bug.getId()));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.status", is(BugStatus.DELETED.name())));
    }

}
