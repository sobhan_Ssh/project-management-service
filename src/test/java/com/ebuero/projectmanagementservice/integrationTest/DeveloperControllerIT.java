package com.ebuero.projectmanagementservice.integrationTest;

import com.ebuero.projectmanagementservice.dto.request.AddDeveloperDTO;
import com.ebuero.projectmanagementservice.dto.request.EditDeveloperDTO;
import com.ebuero.projectmanagementservice.dto.response.DeveloperDTO;
import com.ebuero.projectmanagementservice.entity.Developer;
import com.ebuero.projectmanagementservice.enums.DeveloperStatus;
import com.ebuero.projectmanagementservice.repository.DeveloperRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class DeveloperControllerIT {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private DeveloperRepository developerRepository;
    @Autowired
    private ObjectMapper objectMapper;
    @BeforeEach
    void setup() {
        developerRepository.deleteAll();
    }

    @Test
    public void givenDeveloperObject_whenCreateDeveloper_thenReturnSavedDeveloper() throws Exception {
        // given - create developer dto object
        AddDeveloperDTO developerDTO = new AddDeveloperDTO()
                .setName("Sobhan");

        // when - call '/developer/add' API
        ResultActions response = mockMvc.perform(post("/developer/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(developerDTO)));

        // then - verify the result content using assert statements
        response
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(developerDTO.getName())));
    }

    @Test
    public void givenListOfDevelopers_whenGetAllDevelopers_thenReturnDevelopersList() throws Exception {
        // given - create developers list entity
        List<Developer> developerList = new ArrayList<>();
        developerList.add(new Developer()
                .setName("Sobhan")
                .setStatus(DeveloperStatus.ACTIVE));
        developerList.add(new Developer()
                .setName("Ehsan")
                .setStatus(DeveloperStatus.ACTIVE));
        developerList = developerRepository.saveAll(developerList);

        // when -  call '/developer/list' API
        ResultActions response = mockMvc.perform(get("/developer/list"));

        // then - verify the result list size
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data.length()", is(developerList.size())));
    }

    @Test
    public void givenDeveloperId_whenGetDeveloperById_thenReturnDeveloperObject() throws Exception{
        // given - create developer entity object
        Developer developer = new Developer()
                .setName("Sobhan")
                .setStatus(DeveloperStatus.ACTIVE);
        developer = developerRepository.save(developer);

        // when -  call '/developer/{id}' API
        ResultActions response = mockMvc.perform(get("/developer/{id}", developer.getId()));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.name", is(developer.getName())))
                .andExpect(jsonPath("$.status", is(developer.getStatus().name())))
                .andExpect(jsonPath("$.id", is(developer.getId().intValue())));
    }

    @Test
    public void givenUpdatedDeveloper_whenUpdateDeveloper_thenReturnUpdateDeveloperObject() throws Exception{
        // given - create developer entity object
        Developer developer = new Developer()
                .setName("Sobhan")
                .setStatus(DeveloperStatus.ACTIVE);
        developer = developerRepository.save(developer);

        // given - create edit developer dto
        EditDeveloperDTO editDeveloperDTO = new EditDeveloperDTO()
                .setId(developer.getId())
                .setName("Ehsan");

        // when -  call '/developer' API
        ResultActions response = mockMvc.perform(put("/developer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(editDeveloperDTO)));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.name", is(editDeveloperDTO.getName())));
    }

    @Test
    public void givenDeveloperId_whenDeleteDeveloper_thenReturnDeleteDeveloperObject() throws Exception{
        // given - create developer entity object
        Developer developer = new Developer()
                .setName("Sobhan")
                .setStatus(DeveloperStatus.ACTIVE);
        developer = developerRepository.save(developer);

        // when -  call '/developer' API
        ResultActions response = mockMvc.perform(delete("/developer/{id}", developer.getId()));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.status", is(DeveloperStatus.INACTIVE.name())));
    }
}
