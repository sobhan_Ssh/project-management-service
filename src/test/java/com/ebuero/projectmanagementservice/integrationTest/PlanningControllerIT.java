package com.ebuero.projectmanagementservice.integrationTest;

import com.ebuero.projectmanagementservice.entity.Developer;
import com.ebuero.projectmanagementservice.entity.Story;
import com.ebuero.projectmanagementservice.enums.DeveloperStatus;
import com.ebuero.projectmanagementservice.enums.StoryStatus;
import com.ebuero.projectmanagementservice.repository.ActiveStoryRepository;
import com.ebuero.projectmanagementservice.repository.DeveloperRepository;
import com.ebuero.projectmanagementservice.repository.PlanRepository;
import com.ebuero.projectmanagementservice.repository.StoryRepository;
import com.ebuero.projectmanagementservice.util.Utility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class PlanningControllerIT {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private StoryRepository storyRepository;
    @Autowired
    private DeveloperRepository developerRepository;
    @Autowired
    private ActiveStoryRepository activeStoryRepository;
    @Autowired
    private PlanRepository planRepository;
    @Autowired
    private ObjectMapper objectMapper;
    @BeforeEach
    @AfterEach
    void setup() {
        activeStoryRepository.deleteAll();
        planRepository.deleteAll();
        storyRepository.deleteAll();
        developerRepository.deleteAll();
    }

    @Test
    public void givenListOfDevelopers_givenListOfStories_whenGetPlanning_thenReturnOnePlan() throws Exception {
        // given - create developers list entity
        List<Developer> developerList = new ArrayList<>();
        developerList.add(new Developer()
                .setName("Sobhan")
                .setStatus(DeveloperStatus.ACTIVE));
        developerList.add(new Developer()
                .setName("Ehsan")
                .setStatus(DeveloperStatus.ACTIVE));
        developerList.add(new Developer()
                .setName("Mohsen")
                .setStatus(DeveloperStatus.ACTIVE));
        developerRepository.saveAll(developerList);

        // given - create stories list entity
        List<Story> storyList = new ArrayList<>();
        Story story1 = new Story();
        story1.setCreationDate(Utility.getCurrentDate()).setDesc("desc1").setTitle("title1");
        story1.setEstimatedPoint(10).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story1);
        Story story2 = new Story();
        story2.setCreationDate(Utility.getCurrentDate()).setDesc("desc1").setTitle("title2");
        story2.setEstimatedPoint(10).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story2);
        Story story3 = new Story();
        story3.setCreationDate(Utility.getCurrentDate()).setDesc("desc3").setTitle("title3");
        story3.setEstimatedPoint(10).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story3);
        storyRepository.saveAll(storyList);


        // when -  call '/planning' API
        ResultActions response = mockMvc.perform(get("/planning"));

        // then - verify the result list size
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data.length()", is(1)));
    }

    @Test
    public void givenListOfDevelopers_givenListOfStories_whenGetPlanning_thenReturnThreePlans() throws Exception {
        // given - create developers list entity
        List<Developer> developerList = new ArrayList<>();
        developerList.add(new Developer()
                .setName("Sobhan")
                .setStatus(DeveloperStatus.ACTIVE));
        developerList.add(new Developer()
                .setName("Ehsan")
                .setStatus(DeveloperStatus.ACTIVE));
        developerList.add(new Developer()
                .setName("Mohsen")
                .setStatus(DeveloperStatus.ACTIVE));
        developerRepository.saveAll(developerList);

        // given - create stories list entity
        List<Story> storyList = new ArrayList<>();
        Story story1 = new Story();
        story1.setCreationDate(Utility.getCurrentDate()).setDesc("desc1").setTitle("title1");
        story1.setEstimatedPoint(10).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story1);
        Story story2 = new Story();
        story2.setCreationDate(Utility.getCurrentDate()).setDesc("desc1").setTitle("title2");
        story2.setEstimatedPoint(10).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story2);
        Story story3 = new Story();
        story3.setCreationDate(Utility.getCurrentDate()).setDesc("desc3").setTitle("title3");
        story3.setEstimatedPoint(10).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story3);

        Story story4 = new Story();
        story4.setCreationDate(Utility.getCurrentDate()).setDesc("desc4").setTitle("title4");
        story4.setEstimatedPoint(8).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story4);
        Story story5 = new Story();
        story5.setCreationDate(Utility.getCurrentDate()).setDesc("desc5").setTitle("title5");
        story5.setEstimatedPoint(8).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story5);
        Story story6 = new Story();
        story6.setCreationDate(Utility.getCurrentDate()).setDesc("desc6").setTitle("title6");
        story6.setEstimatedPoint(8).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story6);

        Story story7 = new Story();
        story7.setCreationDate(Utility.getCurrentDate()).setDesc("desc7").setTitle("title7");
        story7.setEstimatedPoint(5).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story4);
        Story story8 = new Story();
        story8.setCreationDate(Utility.getCurrentDate()).setDesc("desc8").setTitle("title8");
        story8.setEstimatedPoint(5).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story8);
        Story story9 = new Story();
        story9.setCreationDate(Utility.getCurrentDate()).setDesc("desc9").setTitle("title9");
        story9.setEstimatedPoint(7).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story9);
        Story story10 = new Story();
        story10.setCreationDate(Utility.getCurrentDate()).setDesc("desc10").setTitle("title10");
        story10.setEstimatedPoint(2).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story10);
        Story story11 = new Story();
        story11.setCreationDate(Utility.getCurrentDate()).setDesc("desc11").setTitle("title11");
        story11.setEstimatedPoint(2).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story11);
        storyRepository.saveAll(storyList);

        // when -  call '/planning' API
        ResultActions response = mockMvc.perform(get("/planning"));

        // then - verify the result list size
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data.length()", is(3)));
    }
}
