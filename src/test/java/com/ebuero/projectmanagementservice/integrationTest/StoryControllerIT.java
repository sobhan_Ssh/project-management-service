package com.ebuero.projectmanagementservice.integrationTest;

import com.ebuero.projectmanagementservice.dto.request.AddBugDTO;
import com.ebuero.projectmanagementservice.dto.request.AddStoryDTO;
import com.ebuero.projectmanagementservice.dto.request.EditBugDTO;
import com.ebuero.projectmanagementservice.dto.request.EditStoryDTO;
import com.ebuero.projectmanagementservice.entity.Bug;
import com.ebuero.projectmanagementservice.entity.Story;
import com.ebuero.projectmanagementservice.enums.BugPriority;
import com.ebuero.projectmanagementservice.enums.BugStatus;
import com.ebuero.projectmanagementservice.enums.StoryStatus;
import com.ebuero.projectmanagementservice.repository.BugRepository;
import com.ebuero.projectmanagementservice.repository.StoryRepository;
import com.ebuero.projectmanagementservice.util.Utility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class StoryControllerIT {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private StoryRepository storyRepository;
    @Autowired
    private ObjectMapper objectMapper;
    @BeforeEach
    void setup() {
        storyRepository.deleteAll();
    }

    @Test
    public void givenStoryObject_whenCreateStory_thenReturnSavedStory() throws Exception {
        // given - create Story dto object
        AddStoryDTO storyDTO = new AddStoryDTO()
                .setTitle("title")
                .setDesc("desc")
                .setEstimatedPoint(10);

        // when - call '/story/add' API
        ResultActions response = mockMvc.perform(post("/story/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(storyDTO)));

        // then - verify the result content using assert statements
        response
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title", is(storyDTO.getTitle())))
                .andExpect(jsonPath("$.desc", is(storyDTO.getDesc())))
                .andExpect(jsonPath("$.estimatedPoint", is(storyDTO.getEstimatedPoint())))
                .andExpect(jsonPath("$.status", is(StoryStatus.ESTIMATED.name())));
    }

    @Test
    public void givenListOfStories_whenGetAllStories_thenReturnStoriesList() throws Exception {
        // given - create stories list entity
        List<Story> storyList = new ArrayList<>();
        Story story1 = new Story();
        story1.setCreationDate(Utility.getCurrentDate()).setDesc("desc1").setTitle("title1");
        story1.setEstimatedPoint(10).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story1);
        Story story2 = new Story();
        story2.setCreationDate(Utility.getCurrentDate()).setDesc("desc2").setTitle("title2");
        story2.setEstimatedPoint(5).setStatus(StoryStatus.ESTIMATED);
        storyList.add(story2);
        storyList = storyRepository.saveAll(storyList);

        // when -  call '/story/list' API
        ResultActions response = mockMvc.perform(get("/story/list"));

        // then - verify the result list size
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data.length()", is(storyList.size())));
    }

    @Test
    public void givenStoryId_whenGetStoryById_thenReturnStoryObject() throws Exception{
        // given - create story entity object
        Story story = new Story();
        story.setCreationDate(Utility.getCurrentDate()).setDesc("desc").setTitle("title");
        story.setEstimatedPoint(10).setStatus(StoryStatus.ESTIMATED);
        story = storyRepository.save(story);

        // when -  call '/story/{id}' API
        ResultActions response = mockMvc.perform(get("/story/{id}", story.getId()));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.title", is(story.getTitle())))
                .andExpect(jsonPath("$.desc", is(story.getDesc())))
                .andExpect(jsonPath("$.estimatedPoint", is(story.getEstimatedPoint())))
                .andExpect(jsonPath("$.status", is(StoryStatus.ESTIMATED.name())));
    }

    @Test
    public void givenUpdatedStory_whenUpdateStory_thenReturnUpdateStoryObject() throws Exception{
        // given - create story entity object
        Story story = new Story();
        story.setCreationDate(Utility.getCurrentDate()).setDesc("desc").setTitle("title");
        story.setEstimatedPoint(10).setStatus(StoryStatus.ESTIMATED);
        story = storyRepository.save(story);

        // given - create edit story dto
        EditStoryDTO editStoryDTO = new EditStoryDTO()
                .setId(story.getId())
                .setTitle("new title")
                .setDesc("new desc")
                .setEstimatedPoint(8);

        // when -  call '/story' API
        ResultActions response = mockMvc.perform(put("/story")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(editStoryDTO)));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.title", is(editStoryDTO.getTitle())))
                .andExpect(jsonPath("$.desc", is(editStoryDTO.getDesc())))
                .andExpect(jsonPath("$.estimatedPoint", is(editStoryDTO.getEstimatedPoint())))
                .andExpect(jsonPath("$.status", is(StoryStatus.ESTIMATED.name())));
    }

    @Test
    public void givenStoryId_whenDeleteStory_thenReturnDeleteStoryObject() throws Exception{
        // given - create bug entity object
        Story story = new Story();
        story.setCreationDate(Utility.getCurrentDate()).setDesc("desc").setTitle("title");
        story.setEstimatedPoint(10).setStatus(StoryStatus.ESTIMATED);
        story = storyRepository.save(story);

        // when -  call '/story' API
        ResultActions response = mockMvc.perform(delete("/story/{id}", story.getId()));

        // then - verify the result content using assert statements
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.status", is(StoryStatus.DELETED.name())));
    }


}
