# Project Management Service

The goal was to develop a very simple REST API for an issue
tracker for a small team of developers.

This is a sample project implemented using below stack:
<ul>
<li>JAVA, as project base language</li>
<li>SPRING-BOOT, as main framework</li>
<li>H2-DATABASE, an in-memory database which needs no configuration</li>
<li>GRADLE, as our build tools and dependency manager</li>
<li>JUNIT & SPRING are also used in order to write integration tests for APIs</li>
</ul>

Based on the project requirements and business logic I have implemented several APIs
which includes: 
<ul>
<li>API services in order to report, update and track BUGS</li>
<li>API services in order to add, update and track STORIES</li>
<li>API services in order to add, update and track DEVELOPERS</li>
</ul>
Also, I have implemented a service to check the active developers and estimated stories
and assign ech developer some tasks up to 10 story-point per plan, and each plan lasts
one week.
<br>
<br>
There is also an export of services for using in Postman application, this file exists in
/resources folder with name of POSTMAN-API-COLLECTION-EXPORT







